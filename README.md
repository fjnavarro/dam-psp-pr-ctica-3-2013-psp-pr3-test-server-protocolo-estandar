# Aplicación en Java - psp-pr3-test-server-protocolo-estandar - Servidor con un protocolo estándar y test clientes con prácticas de programación segura #

*Comparto el siguiente código realizado en el Técnico Superior de Desarrollo de Aplicaciones Multiplataforma.

*Se realizó como una práctica de la asignatura de “Programación de Servicios y Procesos”, es una aplicación Java.

*Hay que tener en cuenta que sólo se pudo utilizar las tecnologías y técnicas dadas en el trimestre de la práctica.

*La práctica se realizó el 14-06-2013

===================


## Texto de la práctica ##

Durante el tercer trimestre hemos visto la arquitectura de servidores y la descripción de varios de los protocolos estándares más habituales.

Realizaremos un servidor que utilice algún protocolo estándar y un test que actuará como cliente.

El funcionamiento del servidor será el siguiente: 

1. Esperará que se produzcan conexiones.

2. Cada conexión entrante creará un hilo que le atenderá, mostrando por consola el desarrollo de la conversación.

3. Se mostrará información sobre el número de clientes conectados.

El funcionamiento del cliente será el siguiente: 

1. Esperará que se introduzcan por teclado el número de clientes que se van a lanzar.

2. Se creará tantos hilos como clientes se van a lanzar, cada uno con un nombre distinto.

3. Con tiempos aleatorios se lanzarán conexiones al servidor con un número aleatorios de mensajes.

4. Se mostrarán por consola el estado de las comunicaciones.

En toda la programación se utilizarán técnicas de programación segura, detallando su uso en la documentación. 

## Nota ##

En esta práctica he usado la versión 1.0 del protocolo HTTP.

Sólo he permitido las peticiones GET, el cliente solicitará archivos que el servidor buscará en su directorio www.

## Screenshots ##

Servidor funcionando

[![Servidor funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr3-test-server-protocolo-estandar/Captura1.png)](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr3-test-server-protocolo-estandar/Captura1.png)

Aplicación test funcionando

[![Aplicación test funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr3-test-server-protocolo-estandar/Captura2.png)](http://fjnavarro.com/assets/screenshot-bitbucket/psp-pr3-test-server-protocolo-estandar/Captura2.png)

## Autor ##

* Francisco José Navarro García <fran@fjnavarro.com>
* Twitter : *[@fjnavarro_](https://twitter.com/fjnavarro_)*
* Linkedin: *[https://www.linkedin.com/in/fjnavarrogarcia](https://www.linkedin.com/in/fjnavarrogarcia)*
* Blog    : *[http://www.fjnavarro.com/](http://www.fjnavarro.com/)*