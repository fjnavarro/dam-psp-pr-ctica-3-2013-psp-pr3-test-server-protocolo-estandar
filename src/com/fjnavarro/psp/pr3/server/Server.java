package com.fjnavarro.psp.pr3.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    int hilos = 1;

        try{
            // Creamos el socket Servidor
	        ServerSocket sk = new ServerSocket(8080);

            while (true){
            	// Activamos el socket para aceptar peticiones de clientes
	     		Socket cliente = sk.accept();

                // Creamos un hilo por cada cliente
	     		HiloClienteWebServer NCl = new HiloClienteWebServer(cliente,hilos);
                
                // Iniciamos el hilo
                NCl.start();
                
                hilos++;
            }
        }catch (IOException e) {
        	e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
        }
    }
}
