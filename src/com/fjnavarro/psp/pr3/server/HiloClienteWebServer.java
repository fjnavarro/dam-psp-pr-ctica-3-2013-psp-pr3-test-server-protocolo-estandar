package com.fjnavarro.psp.pr3.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.Date;
import java.util.Scanner;

public class HiloClienteWebServer extends Thread{
	private Socket cliente;
	private int NumeroHilo;
	final private String wwwDir;

	/* Con el constructor incializamos la clase */
	public HiloClienteWebServer(Socket cliente, int hilo){
        // definimos el socket del cliente 
        this.cliente = cliente;
        // contador de hilos - definimos el n�mero de hilo para diferencias los mensajes
        this.NumeroHilo = hilo;
        
        // modificamos para que el f�n de l�nea se ajuste al est�ndar
 		System.setProperty("line.separator", "\r\n");
 		
 		// obtenemos la ruta donde tenemos el html
        String dir = System.getProperty("user.dir");	        
    	this.wwwDir = dir + File.separator + "www" + File.separator;
    }


    //Funcion utilizada para escribir en el servidor en pantalla
    private void infoHilo(String Var){
        System.out.println("Hilo " + this.NumeroHilo + ": " + Var);
    }

    /* Enviamos el mensaje de respuesta al cliente devolvemos 3 campos */
    public void enviarMensajeHttp(String fichero, PrintWriter escribir, String estado){
    	String linea="";
    	
    	try{
    		// escribimos info de la conexi�n en pantall
    		infoHilo("Inicio Hilo: "+ this.NumeroHilo);
    		infoHilo("IP Cliente conectado:" + this.cliente.getInetAddress().getHostAddress());
            
			// la l�nea de estado
			escribir.println(estado);
				
			// la cabecera
			// obtenemos el mime del fichero
			String mime=null;
			if(fichero.endsWith(".html")||fichero.endsWith(".htm")){
				// es un archivo html
				mime="text/html";
			}else if(fichero.endsWith(".gif")||fichero.endsWith(".htm")){
				// es un archivo de imagen gif
				mime="image/gif";
			}else if(fichero.endsWith(".jpg")||fichero.endsWith(".jpeg")){
				// es un archivo de imagen jpeg
				mime="image/jpeg";
			}else{
				// no tenemos implementado este mime es un archivo por defecto
				mime="application/octet-stream";
			}
			escribir.println("Date: " + new Date());
			escribir.println("Content-Type: "+mime);
			// obtenemos el tama�o del fichero
			File file = new File(this.wwwDir+fichero);
			escribir.println("Content-Length: "+file.length()+2);
			escribir.println();
			
			// el cuerpo			
			// Abrimos el fichero y lo enviamos linea a linea
	        FileReader FRR = new FileReader(this.wwwDir+fichero);
	        BufferedReader Fr = new BufferedReader(FRR);	           
	        while ((linea = Fr.readLine()) != null){
	        	escribir.println(linea);
	        }
	        
	        infoHilo("Archivo devuelto:" + fichero);
	        
	        // Cerramos
	        Fr.close();
	    } catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
    }
    
    // Ejecuci�n del Hilo
    public void run(){
    	try{
	    	// Leemos los elementos del Stream
     		Scanner leer = new Scanner(cliente.getInputStream());
     		// Preparamos la salida del Stream
     		PrintWriter escribir = new PrintWriter(cliente.getOutputStream(),true);
     		
     		// S�lo aceptamos peticiones GET en nuestro servidor
     		if (leer.next().equals("GET")==false){
     			// si la petici�n no es GET
     			
     			// el tipo de petici�n no est� implemtada devolvemos un error 501
     			this.enviarMensajeHttp("501.html", escribir,"HTTP/1.0 501 Not Implemented");
     			
     			//Cierro el cliente
         		leer.close();
         		escribir.close();
                cliente.close();
                
     			// finalizamos
     			return;
     		}
     		
     		// Este debe ser el nombre del fichero
     		String fichero = leer.next();
     		
     		// Comprobamos si existe
     		FileInputStream file = null;
     		boolean existe = true;
     		
     		try{
     			// obtenemos el fichero solicitado
     			file = new FileInputStream(this.wwwDir+fichero);
     		}catch(FileNotFoundException e){
     			// no existe el fichero
     			existe = false;
     		}
     		
     		if(existe && fichero.length()>2){
     			// el fichero existe lo devolvemos	 
     			this.enviarMensajeHttp(fichero, escribir,"HTTP/1.0 200 OK");
     		}else{
     			// el fichero no existe devolvemos un error 404
     			this.enviarMensajeHttp("404.html", escribir,"HTTP/1.0 404 Not Found");
     		}
     		
     		//Cierro el cliente
     		leer.close();
     		escribir.close();
            cliente.close();
            
            // hilo muerto
            infoHilo("Fin Hilo: "+ this.NumeroHilo);
    	} catch (IOException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
    }
    
    
    
    
    
    
    public void pegtote(){
    	
    	
    	
   	 double MilisegundosDesde;
   	 double MilisegundosHasta;
   	 String TxtFromClient="";
   	 String WebHtmlR="";
   	 String linea="";
   	 String UrlFozadaUser="";

               try{
                   //infoHilo("Start Hilo: "+ this.NumeroHilo);
                   //infoHilo("IP Cliente conectado:" + this.cliente.getInetAddress().getHostAddress());

                   // Si la IP es distinta a 192.168.105.25 finalizamos
                   // --------------------------------------------------
                   String IPCliente = cliente.getInetAddress().getHostAddress();
                   String IPAceptada = "127.0.0.1";
                   if (IPCliente.equals(IPAceptada)==false){

                       PrintWriter Cliente_Salida = new PrintWriter(this.cliente.getOutputStream(),true);

                       //Cliente no aceptado. Cerramos conexion
                       WebHtmlR ="<html><head><title>Acceso no permitido - Servidor Jose Luis Bustos</title></head><body><h1>Acceso no permitido a esta pagina por el Servidor de Jose Luis Bustos. CESINE 2011</h1></body></html>";
                       Cliente_Salida.println("HTTP/1.0 200 OK");
                       Cliente_Salida.println("Server: Jose Luis Bustos/1.0");
                       Cliente_Salida.println("Date: " + new Date());
                       Cliente_Salida.println("Content-Type: text/html");
                       Cliente_Salida.println("Content-Length: " + WebHtmlR.length() + 2);
                       Cliente_Salida.println("\n");
                       Cliente_Salida.println(WebHtmlR); //envio una pagina de error controlada

                       this.cliente.close(); //Cerramos conexion con el cliente

                       infoHilo("End Hilo: "+ this.NumeroHilo);

                       return; //Finaliza

                   }

                   //Milisegundos actuales
                   MilisegundosDesde = System.currentTimeMillis();

                    // De entrada del cliente al servidor
                    Scanner Cliente_Entrada = new Scanner(this.cliente.getInputStream());
                    PrintWriter Cliente_Salida = new PrintWriter(this.cliente.getOutputStream(),true);

                   //Metodo cliente peticion y la muestro
                   TxtFromClient = Cliente_Entrada.next();

                   //Si ha intorducido una URL
                   TxtFromClient = Cliente_Entrada.next();

                  //Quitamos la primera / (barra)
                  UrlFozadaUser=TxtFromClient.substring(1);
                  //Controlamos que sea el protocolo http
                  if (UrlFozadaUser.length()>4){
                      if (UrlFozadaUser.substring(0, 4).equalsIgnoreCase("HTTP") == false){
                          UrlFozadaUser = "HTTP://" + UrlFozadaUser;
                      }
                   }
                  infoHilo("Url introducida por usuario " + UrlFozadaUser);


                  try{
                       //Creamos un fichero nuevo para la petici�n

                       FileWriter FW = new FileWriter("DatawebJoselbe_" + this.NumeroHilo + ".txt");
                       BufferedWriter Fs = new BufferedWriter(FW);

                       //Leo la p�gina web remota
                       if (UrlFozadaUser.equals("")){
                           UrlFozadaUser = "http://www.elmundo.es";
                       }

                       URL WebRemota = new URL(UrlFozadaUser);
                       BufferedReader Lectura = new BufferedReader(new InputStreamReader(WebRemota.openStream()));


                       //Enviamos al cliente las lineas de la p�gina
                       while ((linea=Lectura.readLine())!=null){
                            //WebHtmlR = WebHtmlR.concat(linea);
                           Fs.write(linea);
                           Fs.newLine();
                       }

                      //Cerramos Fichero
                      Fs.close();

                       //Abrimos fichero y enviamos linea a linea
                       FileReader FRR = new FileReader("DatawebJoselbe_" + this.NumeroHilo + ".txt");
                       BufferedReader Fr = new BufferedReader(FRR);

                       //usamos para saber el tama�o del fichero
                       File a = new File("DatawebJoselbe_" + this.NumeroHilo + ".txt");


                       //Enviamos OK al Cliente
                       Cliente_Salida.println("HTTP/1.0 200 OK");
                       Cliente_Salida.println("Server: Jose Luis Bustos/1.0");
                       Cliente_Salida.println("Date: " + new Date());
                       Cliente_Salida.println("Content-Type: text/html");
                       Cliente_Salida.println("Content-Length: " + a.length()+2 + WebHtmlR.length() + 30);
                       Cliente_Salida.println("\n");


                       //Enviamos al cliente el fichero
                       while ((linea = Fr.readLine()) != null){
                           Cliente_Salida.println(linea);
                       }

                      //Calculamos tiempo que hemos tardado en hacer la petici�n
                      MilisegundosHasta = System.currentTimeMillis();

                      //A�adimos al final de la p�gina nuestros datos
                      WebHtmlR ="<br><br><b>Servidor Web de Jose Luis Bustos Esteban. Cesine 2011</b> <br> Tiempo de respuesta: ";
                      Cliente_Salida.println(WebHtmlR + (MilisegundosHasta-MilisegundosDesde) + " milisegundos");




                   }
                   catch(Exception e){

                       WebHtmlR ="<html><head><title>Pagina no encontrada - Servidor Jose Luis Bustos</title></head><body><h1>Pagina no encontrada en el servidor de Jose Luis Bustos. CESINE 2011</h1></body></html>";
                       Cliente_Salida.println("HTTP/1.0 200 OK");
                       Cliente_Salida.println("Server: Jose Luis Bustos/1.0");
                       Cliente_Salida.println("Date: " + new Date());
                       Cliente_Salida.println("Content-Type: text/html");
                       Cliente_Salida.println("Content-Length: " + WebHtmlR.length() + 2);
                       Cliente_Salida.println("\n");
                       Cliente_Salida.println(WebHtmlR);
                       infoHilo(e.getMessage());
                   }

                   //Cierro el cliente
                  cliente.close();
                  infoHilo("End Hilo: "+ this.NumeroHilo);
               } //del while


           catch(Exception e){
               infoHilo("Error: " + e.getMessage());
           }
    }

}
