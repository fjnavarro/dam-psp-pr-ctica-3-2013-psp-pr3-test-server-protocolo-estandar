package com.fjnavarro.psp.pr3.client;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class HiloClienteHttp implements Runnable {
	private String nombre;
	private int id;
	private int nMensaje;
	private boolean presentado;
	private String mensaje;
	// preparamos la t�cnica del sem�foro para los hilos
	final Semaphore semaphore = new Semaphore (1);
	
	// a�adimos los archivos a buscar en el servidor web
	private String[] listadoArchivos = { 
		    "index.html","index-1.html","index-2.html",
		    "index-3.html","index-4.html","index-5.html",
		    "index-6.html","index-7.html","style.css",
		    "noExiste.html","tampocoExiste.html"
		};
	
	/* Con el constructor incializamos la clase */
	public HiloClienteHttp(int id){
		// asignamos los datos b�sicos del cliente
		this.id=id;
		this.nombre="test"+id;
		// tenemos un flag para saber si nos hemos presentado al server
		this.presentado=false;
		// ponemos un n�mero de mensajes al azar - lo he limitado a 10 mensajes para no alargar mucho los test
		this.nMensaje=new Double(Math.random() * 10).intValue();
	}
	
	/* M�todo principal de una clase Runnable */
	public void run(){
        try{
        	// enviamos mensajes al servidor mientras que no hayamos terminado con el n�mero de mensajes del hilo actual
        	while(this.nMensaje>0){
		        // enviamos el mensaje al server con el nombre del archivo a buscar
		        this.enviarMensajeHTTP(listadoArchivos[new Double(Math.random() * 11).intValue()]);
		        
		        // quitamos un mensaje de los pendientes a enviar
		        this.nMensaje--;
        	}
        } catch (Exception e) {
        	e.printStackTrace();
	   }
	}
	
	/* M�todo que se encarga de conectar con el server y enviar las preguntas */
	public void enviarMensajeHTTP(String nombreFichero){
		try{
			// inicio del sem�foro en la secci�n cr�tica
			semaphore.acquire();
			
			// Mostramos el hilo actual y el mensaje a enviar al server - as� controlamos es estado de las comunicaciones desde la consola
	        System.out.println(Thread.currentThread()+"---"+nombreFichero);
	        
	        // Creamos el socket cliente
			Socket sk = new Socket("127.0.0.1", 8080);
			
			// Definos los Stream para gestionar el flujo de salida de datos
			OutputStream out = sk.getOutputStream();
			PrintWriter pr = new PrintWriter(out, true);
			
			// Enviamos la petici�n al servidor
			pr.println("GET "+nombreFichero+" HTTP/1.0");
			
			// Obtenemos la respuesta del servidor **S�lo el estado de la petici�n**
     		Scanner leer = new Scanner(sk.getInputStream());
     		System.out.println(Thread.currentThread()+"--- Respuesta del servidor: "+leer.nextLine()); 
     		
			// Cerramos la conexi�n al socket
			sk.close();
			
	        // ponemos a dormir el hilo durante un tiempo aleatorio, de esta forma se va lanzado las conexiones de forma aleatoria
	        Thread.sleep(new Double(Math.random() * 10000).intValue());
	        
	        // fin del sem�foro en la secci�n cr�tica
	        semaphore.release();	        
		}catch(InterruptedException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
